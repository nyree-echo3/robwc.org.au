<?php 
   // Set Meta Tags
   $meta_title_inner = $page->meta_title; 
   $meta_keywords_inner = $page->meta_keywords; 
   $meta_description_inner = $page->meta_description;  
?>

@extends('site/layouts/app')

@section('content')

@include('site/partials/layer-slider-inner')

<div class="panel"> 
	<div class="blog-masthead ">         
		<div class="container">

		  <div class="row">
			@if (count($side_nav) > 1)
			   @include('site/partials/sidebar-pages')        
			@endif

			<div class="col-sm-8 blog-main {{ (count($side_nav) == 1 ? ' blog-main-full' : '') }}">

			  <div class="blog-post">
				@if (isset($page))
				<h1 class="blog-post-title">{{$page->title}}</h1>
				{!! $page->body !!}
				@endif
			  </div><!-- /.blog-post -->         
			</div><!-- /.blog-main -->        

		  </div><!-- /.row -->

		</div><!-- /.container -->
	</div><!-- /.blog-masthead -->
</div><!-- /.panel -->
@endsection
