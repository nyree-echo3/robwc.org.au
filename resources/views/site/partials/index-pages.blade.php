@if(isset($home_products))
	 <div class="home-pages">
	   <div class="container">
		  <div class="row">   
		     @php
			    $counter=0;
			 @endphp
			      
			 @foreach($home_pages as $item)       	 
			      @php
			      $counter++;
			      @endphp
				  <div class="col-lg-4">
		               <div class="home-pages-img">
					      <img class="rounded-circle" src="{{ url('') }}/images/site/pic{{ $counter }}.png" alt="{{ $item["pages"][0]["title"] }}" />
					   </div>
		           
			           <h2>{{ $item["pages"][0]["title"] }}</h2>
			           {!! $item["pages"][0]["short_description"] !!}
			           
			           <p><a class="btn-submit" href="{{ url('') }}/pages/{{ $item["slug"] }}/{{ $item["pages"][0]["slug"] }}" role="button">Learn more</a></p>
				       
				  </div><!-- /.col-lg-4 -->
			 @endforeach 	

			</div>
	   </div>
	</div>
@endif