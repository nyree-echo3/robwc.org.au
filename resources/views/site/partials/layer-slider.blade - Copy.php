	<div id="slider" style="wwidth:1000px;height:556px;margin:0 auto;margin-bottom: 0px;">


		<!-- Slide 1-->
		<div class="ls-slide" data-ls="kenburnsscale:1.2;">
			<img width="1100" height="733" src="http://robwc.test//media/Sliders/4.png" class="ls-l" alt="" style="border: 20px solid #eee; box-shadow: 0px 4px 30px rgba(0,0,0,.3);top:30%; left:50%; text-align:initial; font-weight:400; font-style:normal; text-decoration:none; filter:grayscale(100%) sepia(30%);" data-ls="showinfo:1; offsetxin:left; offsetyin:bottom; durationin:1500; easingin:easeOutQuad; fadein:false; rotatein:10; scalexin:.5; scaleyin:.5; durationout:2000; startatout:allinandloopend + 0; easingout:easeInQuint; loop:true; loopoffsetx:970; loopoffsety:-310; loopduration:3000; loopstartat:transitioninstart + 29000; loopeasing:easeInOutQuart; looprotate:20; loopscalex:1.7; loopscaley:1.7; loopcount:1; loopfilter:grayscale(0%) sepia(0%) contrast(150%); rotation:-20; scaleX:.5; scaleY:.5;">
			<img width="1100" height="825" src="http://robwc.test//media/Sliders/4.png" class="ls-l" alt="" style="border: 20px solid #eee; box-shadow: 0px 4px 30px rgba(0,0,0,.3);top:30%; left:68%; text-align:initial; font-weight:400; font-style:normal; text-decoration:none; filter:grayscale(100%) sepia(30%);" data-ls="showinfo:1; offsetxin:right; offsetyin:top; durationin:1500; delayin:100; easingin:easeOutQuad; fadein:false; rotatein:30; scalexin:.5; scaleyin:.5; durationout:2000; startatout:allinandloopend + 0; easingout:easeInQuint; loop:true; loopoffsetx:-450; loopoffsety:950; loopduration:3000; loopstartat:transitioninstart + 26000; loopeasing:easeInOutQuart; looprotate:-10; loopscalex:1.7; loopscaley:1.7; loopcount:1; loopfilter:grayscale(0%) sepia(0%) contrast(150%); rotation:10; scaleX:.5; scaleY:.5;">
			<img width="769" height="1100" src="http://robwc.test//media/Sliders/4.png" class="ls-l" alt="" style="border: 20px solid #eee; box-shadow: 0px 4px 30px rgba(0,0,0,.3);top:30%; left:86%; text-align:initial; font-weight:400; font-style:normal; text-decoration:none; filter:grayscale(100%) sepia(30%);" data-ls="showinfo:1; offsetxin:right; offsetyin:bottom; durationin:1500; delayin:200; easingin:easeOutQuad; fadein:false; rotatein:30; scalexin:.5; scaleyin:.5; durationout:2000; startatout:allinandloopend + 0; easingout:easeInQuint; loop:true; loopoffsetx:-550; loopoffsety:-650; loopduration:3000; loopstartat:transitioninstart + 23000; loopeasing:easeInOutQuart; looprotate:5; loopscalex:1.7; loopscaley:1.7; loopcount:1; loopfilter:grayscale(0%) sepia(0%) contrast(150%); rotation:-5; scaleX:.5; scaleY:.5;">			
		</div>
	</div>
	
@section('inline-scripts-slider')
<script type="text/javascript">   
    $(document).ready(function(){        
        $("#slider").on('slideTimelineDidComplete', function( event, slider ) {
				slider.api( 'replay' );

			});


			$('#slider').layerSlider({
				sliderVersion: '6.6.4',
				type: 'fullwidth',
				fitScreenWidth: 'true',
				fullSizeMode: 'normal',
				skin: 'v6',
				globalBGImage: '{{ url('') }}/images/site/slider-background.jpg',
				globalBGSize: 'cover',
				showCircleTimer: false,
				allowRestartOnResize: true,
				skinsPath: '../../layerslider/skins/',
				//height: 1000
			});
    });
 
</script>
@endsection