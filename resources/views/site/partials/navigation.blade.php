<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top btco-hover-menu navbar-custom" data-toggle="affix">	
     <div class="navbar-logo">
     <a href="{{ url('') }}" title="{{ $company_name }}"><img src="{{ url('') }}/images/site/logo.png" title="{{ $company_name }}" alt="{{ $company_name }}"></a>
  </div>
  
	
	
	<div class="navbar-container">
		<div class='navbar-contacts'>
			<a href='https://www.paypal.com/donate?business=reachingoutbecausewecan%40hotmail.com&no_recurring=0&item_name=Charity&currency_code=AUD' target="_blank"><img class="paypal-donate" src="{{ url('') }}/images/site/button-PayPal-donate.png" /></a>
			<a href='{{ url('') }}/contact'><i class='far fa-envelope'></i></a>
			@if ( $social_facebook != "") <a href="{{ $social_facebook }}" target="_blank"><i class='fab fa-facebook-f'></i></a> @endif
		</div>
		
		<button class="navbar-toggler custom-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	    </button>

		<div class="collapse navbar-collapse" id="navbarCollapse">
			<ul class="navbar-nav mr-auto">
				<!--<li class="nav-item {{ (!isset($page_type) ? "active" : "") }}">
					<a class="nav-link" href="{{ url('') }}"><i class='fa fa-home'></i> <span class="sr-only">(current)</span></a>
				</li>-->
				@if(count($navigation))
				   @foreach($navigation as $nav_item)    
						<li class="nav-item dropdown">
							<a class="nav-link {{ (isset($page_type) && (($page_type == "Pages" && $category[0]->slug == $nav_item["slug"]) || ($page_type != "Pages" && $page_type == $nav_item["name"])) ? "active" : "") }}" href="{{ url('') }}/{{ ($nav_item["name"] == "Pages" ? "pages/" : "") }}{{ $nav_item["slug"] }}" id="navbarDropdownMenuLink">
								{{ $nav_item["display_name"] }}
							</a>

							@if(isset($nav_item["nav_sub"]) && sizeof($nav_item["nav_sub"]) > 1)
								<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">

									@foreach($nav_item["nav_sub"] as $nav_sub)	

										<li><a class="dropdown-item" href="{{ url('') }}/{{ ($nav_item["name"] == "Pages" ? "pages/" : "") }}{{ $nav_item["slug"] }}/{{ $nav_sub["slug"] }}">{{ (isset($nav_sub["title"]) ? $nav_sub["title"] : $nav_sub["name"]) }}</a>

											@if(isset($nav_item["nav_sub_sub"]))
												<ul class="dropdown-menu">
													@foreach($nav_item["nav_sub_sub"] as $nav_sub_sub)	
													   @if($nav_sub_sub["parent_page_id"] == $nav_sub["id"])														  
														  <li><a class="dropdown-item" href="{{ url('') }}/{{ ($nav_item["name"] == "Pages" ? "pages/" : "") }}{{ $nav_item["slug"] }}/{{ $nav_sub_sub["slug"] }}">{{ (isset($nav_sub_sub["title"]) ? $nav_sub_sub["title"] : $nav_sub_sub["name"]) }}</a></li>
													   @endif
													@endforeach											
												</ul>
											@endif

										</li>
									@endforeach

								</ul>
							@endif

						</li>
				   @endforeach
				@endif

			</ul>	  				
		</div>				
	</div>
</nav>