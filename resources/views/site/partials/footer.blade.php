<footer class='footer'>
 <div class='panel'>
     <!--<hr>-->
     
	 <div class='footerContacts'>
        <div class="panelNav">
	    <div class="container-fluid">
	    <!--
		<div class="row">
       
           <div class="col-lg-2 col-full">
		      <div class="center-block">
			     <a href="{{ url('') }}" title="{{ $company_name }}"><img src="{{ url('') }}/images/site/logo.png" title="{{ $company_name }}" alt="{{ $company_name }}"></a>
			  </div>
		   </div> 
		   
		   <div class="col-lg-4 col-full">
		   </div>  
      
           <div class="col-lg-2 col-full">
		      <div class="center-block">
			      @if(count($modules))	
			           @php
			           $counter=0;
			           @endphp	
			           			  
					   @foreach($modules as $nav_item)    
					        @php
					        $counter++;
					        
					        if ($counter == 4) {
					           break;
					        }
					        @endphp
					        
							<div class="footer-txt footer-menu"><a href="{{ url('') }}/{{ ($nav_item["name"] == "Pages" ? "pages/" : "") }}{{ $nav_item["slug"] }}">{{ $nav_item["display_name"] }}</a></div>
					   @endforeach
				   @endif
			   </div>
		   </div>  
       
	       <div class="col-lg-2 col-full">	
	          <div class="center-block">
			      @if(count($modules))	
			           @php
			           $counter=0;
			           @endphp
			           				  
					   @foreach($modules as $nav_item) 
				           @php
					        $counter++;
					       @endphp
					         
					       @if ($counter > 3)
							   <div class="footer-txt footer-menu"><a href="{{ url('') }}/{{ ($nav_item["name"] == "Pages" ? "pages/" : "") }}{{ $nav_item["slug"] }}">{{ $nav_item["display_name"] }}</a></div>
						   @endif	
					   @endforeach
				   @endif
			   </div>		  
		   </div>  		   		   				   		   
		   
		   <div class="col-lg-2 col-full">			  
			  <div class="center-block">		    
			    <div class="footer-txt footer-menu"><a href="{{ url('') }}/contact">Contact us&nbsp;&nbsp;&nbsp;<i class='far fa-envelope'></i></a></div>						
				@if ( $social_facebook != "") <div class="footer-txt footer-menu"><a href="{{ $social_facebook }}" target="_blank">Follow us&nbsp;&nbsp;&nbsp;<i class='fab fa-facebook-f'></i></a></div>@endif 						
			  </div>
		   </div>  
		   		   		   
		</div>
		-->
	 </div> 
 </div>
 </div>
  </div>
   
 <div class='footerContainer'>  
  <div class="footer-support">
     <div>
        This site is lovingly sponsored by
        <a href="https://www.echo3.com.au" title="Echo3"><img src='{{ url('') }}/images/site/echo3 logo.png' title="Echo3" alt="Echo3" ></a> 
     </div>
  </div>
  
  </div>
</footer>